package oop.asg04;

import java.awt.*;
import javax.swing.*;

import java.util.*;
import java.awt.event.*;
import javax.swing.event.*;

import java.awt.Toolkit;


class JBrainTetris extends JTetris {
	protected JCheckBox brainMode;
	protected	DefaultBrain dbrain;
	protected Brain.Move  move;
	protected JSlider adversary;
	
	public JBrainTetris(int pixels){
		super(pixels);
	
	}


	public JComponent createControlPanel(){
		JPanel p = (JPanel)super.createControlPanel();
		p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));
		p.add(new JLabel("Brain:"));
		brainMode = new JCheckBox("Brain active");
		p.add(brainMode);
		p.add(new JLabel("Adversary:"));
		adversary = new JSlider(0, 100, 0); // min, max, current
		adversary.setPreferredSize(new Dimension(100,15));
		p.add(adversary);
		// now add p to panel of controls
 
		return p;
	}
	public void tick(int verb){
		
		if(brainMode.isSelected()){
			dbrain = new DefaultBrain();
			if(currentPiece!=null) {
				board.undo();
				move = dbrain.bestMove(board,currentPiece,board.getHeight(),move);
			}
		
			if(!currentPiece.equals(move.piece))
				super.tick(ROTATE);
			else if(currentX>move.x)
				super.tick(LEFT);
			else if(currentX<move.x)
				super.tick(RIGHT);
			super.tick(verb);
		}
		else super.tick(verb);
	}
	public void addNewPiece(){
		super.addNewPiece();
	}
	public static void main(String args[]) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception ignored) { }
		
		JBrainTetris tetris = new JBrainTetris(16);
		JFrame frame = JBrainTetris.createFrame(tetris);
		frame.setVisible(true);
	}

}